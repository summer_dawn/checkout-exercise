# Checkout exercise

## Running the app

- Make sure you have `node`, `npm`, and `yarn` installed on your machine.
- Clone the repo
- Run `yarn` to install dependencies
- Run `yarn serve` to build the app and serve it on `localhost`

## Libraries used

- `react`: UI and State management
- `parcel`: very light JS bundler and web-server
- `lodash`: collection of utilities for functional programming

## Comments

### Folder structure

I've decided to structure folders by features rather than by object type, as this makes it easier to navigate the codebase and isolate its components. Please search `folders-by-features` for a quick overview of the pattern

### Testing

In a real world scenario this repo would require at the very least unit and functional tests. 

The first candidate for unit testing is  `src/shop/model/cart`, which contains the core business logic. `src/shop/components/CheckoutForm` would also benefit from testing.

### Tooling

I've decided to use `parcel` to create, run and manage the project. The main benefit over `create-react-app` is support for Typescript, easier configuration and smaller footprint. I've considered to use `nx` as well, but ended up chooisng `parcel` as it's the most lightweight.


### Coding standards

I've decided to implement the requirements using a functional approach.

The main reason is that functional programming pairs itself very well with React mental models and features, as almost everything in the framework is expressed in a functional style.

Another reason is that using a data-driven functional approach results in a very small code footprint, as every bit of the codebase deals with data transformation, which can be expressed very effetcively with `lodash` and core Typescript capabilities.

It also unlocks performance benefits, as every data-manipulation function has been implement with Time Complexity of O(N), as described in the comments.


### Caveats

#### Linter and Formatter

I've not implemented any linting or formatting apart from `tsc` defaults.  Apart from the lack of time, I've kept non-standard identation and formatting to help reading rather than extending.

#### Functional vs Object Oriented Programming

It's worth noting that Functional Programming (FP) is not as widespread as Object Oriented Programming (OOP), even in the React community; so an argument can be made that in a shared codebase not everyone might be familiar with FP. 

However, a first attempt in modelling the problem in OOP (not visible due to commit squashing) has resulted in a larger code footprint, less declarative code and worse performance due to multiple nested iterations required. Hence the choice to stick with FP.

#### Lack of testing

I've decided to do without testing due to lack of time. If this was a real-life project, I would reccommend writing tests for the two paths mentioned above before deploying to production.

#### Styling

I've not used CSS or any styling at all due to time constraints. In a real world implementation, even simple functionalities like the ones implemented should have proper styling.

#### CI/CD

Due to lack of time I've not implemented any CI/CD. It's however trivial to implement both with Gitlab and GitHub, as it would be enough to run `test`, `lint` and `build` on every commit, and upload the resulting blob on any hosting platform. 


# Requirements

## Step 1: Shopping cart

You are building a checkout system for a shop which only sells `apples` and `oranges`.

Apples cost `60p` and oranges cost `25p`.
Build a checkout system which takes a list of items scanned at the till and outputs
the total cost

For example: `[ Apple, Apple, Orange, Apple ] => £2.05`

Make reasonable assumptions about the inputs to your solution; for example, many
candidates take a list of strings as input.


## Step 2: Simple offers

The shop decides to introduce two new offers:
- buy one, get one free on Apples
- 3 for the price of 2 on Oranges

Update your checkout functions accordingly.