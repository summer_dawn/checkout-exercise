import React, { useState } from "react";

import { checkOut } from '../model/cart'
import { PriceMap, OfferMap } from "../model/types";

type Props = {
    priceMap: PriceMap,
    offerMap?: OfferMap,
}

const CheckoutForm = ({ priceMap, offerMap }: Props) => {
    const [cartItems, setCartItems] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const [offersEnabled, setOffersEnabled] = useState(false)

    // Please note, this doesn't do input-checking as not required. In a real world scenario, we probably would want to add input validation here.
    const handleCartInput = (event) => {
        setCartItems(event.target.value.split(", "))
    }

    // Same as above, this assumes the input is correct. If any input is not correct, it will return NaN.
    const handleSubmit = () => {
        const totalPrice = checkOut({ items: cartItems }, priceMap, offersEnabled ? offerMap : null);
        setTotalPrice(totalPrice)
    }

    const handleOfferInput = (event) => {
        setOffersEnabled(event.target.checked)
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        handleSubmit();
    }

    // In a real world scenario, it would be useful to show subtotal as well. 
    // Not implemented here as it's not present in requirements.
    return <div>
        <form onSubmit={handleFormSubmit}>
            <label >
                Please write item list below with comma separated values <br />
                <input type="text" name="cart" onChange={handleCartInput} />
                <br />
            </label>
            <label >
                Tick to enable offers <br />
                <input type="checkbox" name="offer" onChange={handleOfferInput} />
            </label>
            <br />
            <input type="button" value="Submit" onClick={handleSubmit} />
        </form>
        <h1>
            Total price: {totalPrice}
        </h1>
    </div>
}




export default CheckoutForm;