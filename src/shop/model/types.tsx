export type Apple = "Apple"
export type Orange = "Orange"

export type Product = Apple | Orange

export type Price = number; // Define Price type explicilty to enable multi-currency support in a further iteration.

export type PriceMap = {
    [K in Product]: Price
}

export type OfferMap  = {
    [K in Product]?: Offer
}

export type Offer = (priceMap: PriceMap) => (itemCount: number) => Price 



// In a real-life scenario, this could be expanded to have an Id, an Owner and various other properties
export interface Cart {
    items: Product[],
}

export type CheckOut = (cart: Cart, priceMap: PriceMap, offersMap?: OfferMap) => Price