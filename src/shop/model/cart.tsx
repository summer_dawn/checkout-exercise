import { CheckOut, Cart, Product, PriceMap, OfferMap } from "./types";
import { countBy, entries, get } from 'lodash'

// n.b. we define ItemCount this way because `entries` has this return type, and it cannot be cast to Array<[Product, number]>
type ItemCount = Array<[String, number]>

// Populate a dictionary with a count of each product occurence, then return its entries
// e.g. ["Apple", "Apple", "Orange", "Orange", "Orange"] => { "Apple": 2, "Orange": 3 } => [["Apple", "2"], ["Orange", "3"]]
const getItemsCount: (cart: Cart) => ItemCount = (cart) => entries(countBy(cart.items, (item) => item))


const isItemInOffer = (offersMap: OfferMap, productType: Product) => get(offersMap, productType);

const calculateSubtotal = (productType, priceMap) => productQuantity => priceMap[productType] * productQuantity // Simplyulitply unit price per quantity

const getSubtotalFunction = (productType: Product, priceMap: PriceMap, offersMap?: OfferMap) =>
    isItemInOffer(offersMap, productType) ?
        offersMap[productType](priceMap) : // Defer subtotal calculation to offer function
        calculateSubtotal(productType, priceMap)


// n.b. `[productType, productQuantity]` is defined as `any` as it's not possible to accept `[Product, number]` due to the return type of `entries`
const reduceItemsToTotal = (priceMap: PriceMap, offersMap?: OfferMap) =>
    (preTotal: number, [productType, productQuantity]) =>
        preTotal + getSubtotalFunction(productType, priceMap, offersMap)(productQuantity)


const getTotal = (itemCount: ItemCount, priceMap: PriceMap, offersMap?: OfferMap) =>
    itemCount.reduce(reduceItemsToTotal(priceMap, offersMap), 0);


export const checkOut: CheckOut = (cart: Cart, priceMap: PriceMap, offersMap?: OfferMap) => getTotal(getItemsCount(cart), priceMap, offersMap)


/*
    Time complexity analysis of the above functions:

    `countBy`         =>  O(N) (https://github.com/lodash/lodash/blob/2f79053d7bc7c9c9561a30dda202b3dcd2b72b90/countBy.js)
    `entries`         =>  O(N) (https://github.com/lodash/lodash/blob/4.17.15/lodash.js#L1178)
    `get`             =>  O(1) (Key lookup in Typescript is O(1) )

    `reduce`          =>  O(N) (It's a simple for loop under the hood)
    `object[key]`     =>  O(1) (Key lookup in Typescript is O(1) )

    `getItemsCount`   =>  `entries` + `countBy`             => O(N) + O(N) = O(N)

    `getTotal`        =>  `reduce` + `get` + `object[key]`  => O(N) + O(N) + O(N) = O(N)

    `checkOut`        => `getTotal` + `getItemsCount`       => O(N) + O(N) = O(N)
*/
