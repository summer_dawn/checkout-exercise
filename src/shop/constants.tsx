import { OfferMap, PriceMap, Offer } from "./model/types";
import { range } from 'lodash'


export const PRICE_MAP: PriceMap = {
    "Apple": 0.6,
    "Orange": 0.25,
}

const checkIsEven = n => n % 2 === 0;

const checkIsThirdInTriplet = n => n % 3 === 2;


// Only add price to the accumulator if item index is even. (0*, 1, 2*, 3)
const appleOffer: Offer = (priceMap: PriceMap) => (itemCount: number) =>
    range(itemCount).reduce((accumulator, currentValue) =>
        checkIsEven(currentValue) ? accumulator + priceMap["Apple"] : accumulator, 0)


// Only add price to the accumulator if item is NOT third in the triplet (0*, 1*, 2, 3*, 4*, 5 )
const orangeOffer = (priceMap: PriceMap) => (itemCount: number) =>
    range(itemCount).reduce((accumulator, currentValue) =>
        !checkIsThirdInTriplet(currentValue) ? accumulator + priceMap["Orange"] : accumulator, 0);


export const OFFER_MAP: OfferMap = {
    "Apple": appleOffer,
    "Orange": orangeOffer,
}