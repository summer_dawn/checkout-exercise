import React from "react";
import { render } from "react-dom";

import Checkout from './shop/components/CheckoutForm'
import { PRICE_MAP, OFFER_MAP } from "./shop/constants"

render(<Checkout offerMap={OFFER_MAP} priceMap={PRICE_MAP} />, document.getElementById("root"));